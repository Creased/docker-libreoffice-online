# Libreoffice Online docker-compose wrapper

## Setup ##

### Requirements ###

- [Docker](https://docs.docker.com/engine/installation/);
- [Docker Compose](https://docs.docker.com/compose/install/).

### Download ###

```bash
git clone [URL] lool
pushd lool/
```

### Build ###

Build of containers based on docker-compose.yml:

```bash
docker-compose build
```

## Start ##

To get it up, please consider using:

```bash
docker-compose create --force-recreate
docker-compose up -d
```

## Live display of logs ##

```bash
docker-compose logs --follow
```

## Run bash on container ##

Template:

```bash
docker-compose exec SERVICE COMMAND
```

Example:

```bash
docker-compose exec libreoffice bash
```

Then you will be able to manage your configuration files, debug daemons and much more...
