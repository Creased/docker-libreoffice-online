#
# LibreOffice Online Dockerfile
#
# Written by:
#   Baptiste MOINE <contact@bmoine.fr>
#

# Pull base image.
FROM libreoffice/online:master

MAINTAINER Baptiste MOINE <contact@bmoine.fr>

# Make LOOL script executable.
RUN chmod +x /run-lool.sh

# Create volumes.
VOLUME ["/etc/loolwsd"]

# Set workdir.
WORKDIR /

# TCP port that container will listen for connections.
# HTTP and HTTPS.
EXPOSE 9980/tcp

CMD ["sh", "-c", "/run-lool.sh"]
